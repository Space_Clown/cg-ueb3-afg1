
# pip3 install PyOpenGL
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

# pip3 install Pillow==8.3.1
from PIL import Image

import numpy as np
from const import E
from vec3 import vec3

from time import sleep

from raytrace import raytrace

from const import C

class Scene:
    """
        OpenGL 2D scene class that render a textured quad.
        DO NOT CHANGE ANY OF THE GIVEN FUNCTIONS EXCEPT raytrace_image()
    """

    # initialization
    def __init__(self, width, height, objects, scenetitle="2D Scene"):
        # time
        self.scenetitle = scenetitle
        self.width = width
        self.height = height
        self.texture_id = None
        self.objects = objects
        self.rotation = 0
        self.doRotation = False

    def initialize_image(self):
        # initialize a texture object with empty (black) rgb image
        image_data = np.zeros((self.width, self.height, 3), dtype=np.uint8)
        w, h, d = image_data.shape
        image_data = image_data.reshape((w * h, d))
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
        self.texture_id = glGenTextures(1)
        glBindTexture(GL_TEXTURE_2D, self.texture_id)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0)
        # allocate texture for the first time
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, image_data)

    # TODO error by resize coursed here at glTexSubImage2D -> but why :(
    def update_img(self, image_data):
        # update texture by replace the image data of the texture object
        # using glTextSubImage2D(...)
        w, h, d = image_data.shape
        image_data = image_data.reshape((w * h, d))

        glEnable(GL_TEXTURE_2D)
        glBindTexture(GL_TEXTURE_2D, self.texture_id)
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, w, h, GL_RGB, GL_UNSIGNED_BYTE, image_data)
        # draw textured rectangle
        glBegin(GL_TRIANGLE_STRIP)
        for vt in np.array([(0, 0), (0, 1), (1, 0), (1, 1)]):
            glTexCoord2fv(vt)
            glVertex2fv(vt * np.array([self.width,self.height]))
        glEnd()
        glDisable(GL_TEXTURE_2D)

    def render(self):
        # if no texture_id is available (first call of render) initialize
        if not self.texture_id:
            self.initialize_image()
        else:
            image = self.render_image()
            self.update_img(image)


    def render_image(self):

        r = float(self.width) / self.height
        # Screen coordinates: x0, y0, x1, y1.
        S = (-1, 1 / r + .25, 1, -1 / r + .25)
        x = np.tile(np.linspace(S[0], S[2], self.width), self.height)
        y = np.repeat(np.linspace(S[1], S[3], self.height), self.width)

        Q = vec3(x, y, 0) #center

        if self.doRotation:
            for i in range(len(self.objects)):
                self.objects[i] = self.objects[i].rotate(C, self.rotation)
            self.doRotation = False

        image = self.renderObjects(E, (Q - E).norm())
        return image

    def renderObjects(self, O, D):
        color = raytrace(O, D, self.objects )
        rgb = [Image.fromarray((255 * np.clip(c, 0, 1).reshape((self.height, self.width))).astype(np.uint8), "L") for c in
               color.components()]
        image = Image.merge("RGB", rgb)
        image_sequence = image.getdata()
        image_array = np.array(image_sequence)

        # reformat the array
        image_formatted = image_array.reshape(self.width, self.height, 3)
        return image_formatted
