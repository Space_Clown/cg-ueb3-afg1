from vec3 import vec3

FARAWAY = 1.0e39

C = vec3(0, 0, 3)           # Scene Center
L = vec3(10, 5, -10)         # Point light position
E = vec3(0, 0.35, -1)       # Eye position

# L = C + vec3(5, 5, 5)       # Point light position
# E = C + vec3(0, 0, 5)       # Eye position

rgb = vec3