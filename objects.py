import numpy as np
from const import FARAWAY, L, E, rgb
from functools import reduce

from raytrace import raytrace
from vec3 import vec3
from rotate import rotate

class Sphere:
    def __init__(self, center, r, diffuse, mirror):
        self.c = center
        self.r = r
        self.diffuse = diffuse
        self.mirror = mirror

    def intersect(self, O, D):
        b = 2 * D.dot(O - self.c)
        c = abs(self.c) + abs(O) - 2 * self.c.dot(O) - (self.r * self.r)
        disc = (b ** 2) - (4 * c)
        sq = np.sqrt(np.maximum(0, disc))
        h0 = (-b - sq) / 2
        h1 = (-b + sq) / 2
        h = np.where((h0 > 0) & (h0 < h1), h0, h1)
        pred = (disc > 0) & (h > 0)
        return np.where(pred, h, FARAWAY)

    def diffusecolor(self, M):
        return self.diffuse

    def light(self, O, D, d, scene, bounce):
        M = (O + D * d)                         # intersection point
        N = (M - self.c) * (1. / self.r)     # normal
        toL = (L - M).norm()                    # direction to light
        # E -> Eye position
        toO = (E - M).norm()                    # direction to ray origin
        nudged = M + N * .0001                  # M nudged to avoid itself

        # Shadow: find if the point is shadowed or not.
        # This amounts to finding out if M can see the light
        light_distances = [s.intersect(nudged, toL) for s in scene]
        light_nearest = reduce(np.minimum, light_distances)
        seelight = light_distances[scene.index(self)] == light_nearest

        # Ambient
        color = rgb(0.05, 0.05, 0.05)

        # Lambert shading (diffuse)
        lv = np.maximum(N.dot(toL), 0)
        print()
        color += self.diffusecolor(M) * lv * seelight

        # Reflection
        if bounce < 2:
            rayD = (D - N * 2 * D.dot(N)).norm()
            color += raytrace(nudged, rayD, scene, bounce + 1) * self.mirror

        # Blinn-Phong shading (specular)
        phong = N.dot((toL + toO).norm())
        color += rgb(1, 1, 1) * np.power(np.clip(phong, 0, 1), 50) * seelight
        return color

    def rotate(self, c, alpha):
        center = vec3(*rotate(self.c, c, alpha))
        return self.__class__(center, self.r, self.diffuse, self.mirror)

class CheckeredSphere(Sphere):
    def diffusecolor(self, M):
        checker = ((M.x * 2).astype(int) % 2) == ((M.z * 2).astype(int) % 2)
        return self.diffuse * checker


class Triangle:
    def __init__(self, pA, pB, pC, diffuse, mirror):
        self.pA = pA
        self.pB = pB
        self.pC = pC
        self.vAB = (pB - pA)
        self.vAC = (pC - pA)
        self.diffuse = diffuse
        self.mirror = mirror

    def diffusecolor(self, M):
        return self.diffuse

    def intersect(self, O, D):
        w = (O - self.pA)
        v = self.vAB
        u = self.vAC
        basicPart = 1/(D.cross(v).dot(u))

        t = w.cross(u).dot(v) * basicPart
        r = D.cross(v).dot(w) * basicPart
        s = w.cross(u).dot(D) * basicPart

        pred = (0 <= r) & (r <= 1) & (0 <= s) & (s <= 1) & (r+s <=1)
        return np.where(pred, t, FARAWAY)

    def light(self, O, D, d, scene, bounce):
        M = (O + D * d)  # intersection point
        N = self.vAB.cross(self.vAC).norm() * -1 # normal, -1 weil die richtung invertiert werden muss.
        toL = (L - M).norm() # direction to light
        # E -> Eye position
        toO = (E - M).norm()# direction to ray origin
        nudged = M + N * .0001  # M nudged to avoid itself

        # Shadow: find if the point is shadowed or not.
        # This amounts to finding out if M can see the light
        light_distances = [s.intersect(nudged, toL) for s in scene]
        light_nearest = reduce(np.minimum, light_distances)
        seelight = light_distances[scene.index(self)] == light_nearest

        # Ambient
        color = rgb(0.05, 0.05, 0.05)

        # Lambert shading (diffuse)
        lv = np.maximum(N.dot(toL), 0)
        color += self.diffusecolor(M) * lv * seelight

        # Reflection
        if bounce < 2:
            rayD = (D - N * 2 * D.dot(N)).norm()
            color += raytrace(nudged, rayD, scene, bounce + 1) * self.mirror

        # Blinn-Phong shading (specular)
        phong = N.dot((toL + toO).norm())
        color += rgb(1, 1, 1) * np.power(np.clip(phong, 0, 1), 50) * seelight
        return color

    def rotate(self, c, alpha):
        pA = vec3(*rotate(self.pA, c, alpha))
        pB = vec3(*rotate(self.pB, c, alpha))
        pC = vec3(*rotate(self.pC, c, alpha))
        return self.__class__(pA, pB, pC, self.diffuse, self.mirror)
