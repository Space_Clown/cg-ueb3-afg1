import glfw

# pip3 install PyOpenGL
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *


from vec3 import vec3

from objects import Sphere, CheckeredSphere, Triangle

from const import rgb, C

from scene import Scene
import numpy as np



class RenderWindow:
    """
        GLFW Rendering window class
        YOU SHOULD NOT EDIT THIS CLASS!
    """

    def __init__(self, scene):

        # save current working directory
        cwd = os.getcwd()

        # Initialize the library
        if not glfw.init():
            return

        # restore cwd
        os.chdir(cwd)

        # version hints
        # glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
        # glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 3)
        # glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, GL_TRUE)
        # glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)
        # glfw.window_hint(glfw.COCOA_RETINA_FRAMEBUFFER, glfw.TRUE)
        # buffer hints
        glfw.window_hint(glfw.DEPTH_BITS, 32)

        # make a window
        self.width, self.height = scene.width, scene.height
        self.aspect = self.width / float(self.height)
        self.window = glfw.create_window(self.width, self.height, scene.scenetitle, None, None)
        if not self.window:
            glfw.terminate()
            return

        # Make the window's context current
        glfw.make_context_current(self.window)

        # initialize GL
        self.initGL()

        # set window callbacks
        # glfw.set_mouse_button_callback(self.window, self.onMouseButton)
        glfw.set_key_callback(self.window, self.onKeyboard)
        glfw.set_window_size_callback(self.window, self.onSize)
        # hack to avoid framebuffer size error
        x, y = glfw.get_window_pos(self.window)
        glfw.set_window_pos(self.window, x+1, y+1);

        # create scene
        self.scene = scene  # Scene(self.width, self.height)

        # exit flag
        self.exitNow = False

    def initGL(self):
        # initialize OpenGL
        glEnable(GL_BLEND)
        glClear(GL_COLOR_BUFFER_BIT)
        glViewport(0, 0, self.width, self.height)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glOrtho(0.0, float(self.width), float(self.height), 0.0, 0.0, 1.0)

    def onKeyboard(self, win, key, scancode, action, mods):
        print("keyboard: ", win, key, scancode, action, mods)
        if action == glfw.PRESS:
            # ESC to quit
            if key == glfw.KEY_ESCAPE:
                self.exitNow = True
            elif key == glfw.KEY_P:
                self.scene.rotation = np.pi / 24
                self.scene.doRotation = True
            elif key == glfw.KEY_N:
                self.scene.rotation = - np.pi / 24
                self.scene.doRotation = True
            elif key == glfw.KEY_UP:
                print("Up")


    def onSize(self, win, width, height):
        print("onsize: ", win, width, height)
        self.width = width
        self.height = height
        self.aspect = width / float(height)
        glViewport(0, 0, self.width, self.height)
        # hack to avoid framebuffer size error
        x, y = glfw.get_window_pos(self.window)
        glfw.set_window_pos(self.window, x+1, y+1)
        self.scene.width = width
        self.scene.height = height


    def run(self):
        while not glfw.window_should_close(self.window) and not self.exitNow:
            glfw.poll_events()
            glClear(GL_COLOR_BUFFER_BIT)
            self.scene.render()
            glfw.swap_buffers(self.window)
        # end
        glfw.terminate()


# main function
if __name__ == '__main__':
    # set size of render viewport
    width, height = 1013, 480

    objects = [
        Triangle(C + vec3(0, 1.1, 1), C + vec3(-.75, .1, 1), C + vec3(0.75, .1, 1), rgb(.3, .9, .4), 0.1),
        Triangle(C + vec3(-2, 2, 2), C + vec3(-2.75, .1, 2), C + vec3(-0.75, .1, 2), rgb(.3, .9, .4), 0.1),
        Sphere(C + vec3(0.75, .1, 1), .6, rgb(0, 0, 1), 0.01),
        Sphere(C + vec3(-.75, .1, 1), .6, rgb(.5, .223, .5), 0.1),
        Sphere(C + vec3(0, 1.1, 1), .6, rgb(1, .772, .184), 0.9),
        CheckeredSphere(vec3(0, -99999.5, 0), 99999, rgb(.75, .75, .75), 0.25),
    ]

    # instantiate a scene
    scene = Scene(width, height, objects, "Raytracing Template",)

    # pass the scene to a render window ...
    rw = RenderWindow(scene)

    # ... and start main loop
    rw.run()