import numpy as np

def rotate(p, c, alpha):
    mat_t1 = np.array([
        [1, 0, 0, -c.x],
        [0, 1, 0, -c.y],
        [0, 0, 1, -c.z],
        [0, 0, 0, 1]
    ])

    mat_t2 = np.array([
        [1, 0, 0, c.x],
        [0, 1, 0, c.y],
        [0, 0, 1, c.z],
        [0, 0, 0, 1]
    ])

    mat_r = np.array([
        [np.cos(alpha), 0, -np.sin(alpha), 0],
        [0, 1, 0, 0],
        [np.sin(alpha), 0, np.cos(alpha), 0],
        [0, 0, 0, 1]
    ])

    point = np.array([*p.components(), 1]).T

    return mat_t2.dot(mat_r.dot(mat_t1.dot(point)))[:3]